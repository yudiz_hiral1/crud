import React, { Component } from 'react'
import './form.css'
/* eslint-disable */

export default class Form extends Component {
  constructor(props) {
    super(props)
    this.state = {
      cityData: [],
      name: '',
      city: '',
      buttonStatus: 'Add',
      editIndex: '',
    }
  }

  handleInput = (e, type) => {
    if (type === 'name') {
      this.setState({
        cityData: [...this.state.cityData],
        city: this.state.city,
        name: e.target.value,
      })
    }
    if (type === 'city') {
      this.setState({
        cityData: [...this.state.cityData],
        name: this.state.name,
        city: e.target.value,
      })
    }
  }

  submitAdd = (event) => {
    if (this.state.buttonStatus === 'Add') {
      this.setState({
        cityData: [...this.state.cityData,
        {
          name: this.state.name,
          city: this.state.city
        },
        ],
      })
    }
    if (this.state.buttonStatus === 'Update') {
      const tempCity = this.state.cityData
      tempCity.splice(this.state.editIndex, 1, {
        name: this.state.name,
        city: this.state.city,
      })
      this.setState({ cityData: tempCity, buttonStatus: 'Add', editIndex: '' })
      
    }
    this.setState({name:'',city: ''})
    event.preventDefault()
  }

  deleteCity = (index) => {
    const tempCity = this.state.cityData
    tempCity.splice(index, 1)
    this.setState({ cityData: tempCity })
  }

  updateButton = (index) => {
    this.setState({
      cityData: [...this.state.cityData],
      name: this.state.cityData[index].name,
      city: this.state.cityData[index].city,
      buttonStatus: 'Update',
      editIndex: index,
    })
  }

  componentDidMount() {
    this.state.cityData
  }

  componentDidMount() {
    this.state.buttonStatus
  }

  render() {
    return (
      <div className='container'>
        <div className='title'>Form</div>
        <br />
        <form id='login_form' method='' action='#' name='regi_form'>
          <div className='user-details'>

            <div className='input-box'>
              <span className='details'>Name</span>
              <input type='text' id='fullname' name='fullname' placeholder='Enter Name' value={this.state.name}
                onChange={(e) => this.handleInput(e, 'name')} required />
            </div>

            <div className='input-box'>
              <span className='details'>City</span>
              <input type='text' id='city' name='city' placeholder='Enter City' value={this.state.city}
                onChange={(e) => this.handleInput(e, 'city')} required />
            </div>
          </div>

          <div className='button'>
            <input type='submit' value={this.state.buttonStatus} className='btn' onClick={this.submitAdd} />
          </div>
        </form>
        <div>
          <table cellSpacing={25} cellPadding={25} >
            <thead>
              <tr>
                <th>Index</th>
                <th>Name</th>
                <th>City</th>
                <th>Update</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
              {this?.state?.cityData.map((value, index) => {
                return (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{value.name}</td>
                    <td>{value.city}</td>
                    <th>
                      <button onClick={(e) => this.updateButton(index)}> Update </button>
                    </th>
                    <th>
                      <button onClick={(e) => this.deleteCity(index)}> Delete </button>
                    </th>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}
